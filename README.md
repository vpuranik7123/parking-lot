# gojek -Parking Lot Assignment

#Build Instructions

From the project directory, Run the following command -

$ mvn clean install

Once the build is success, you'll get the gojek-v1.jar generated in the target folder. 



#Running the project

From the project directory, run this command - 

For  Interactive CLI based input:

   $ java -jar target/gojek-v1.jar
   
    
For File based Input: 

   $ java -jar target/gojek-v1.jar "inputfile.txt path" 

   
# To run this in linux environment:

Run the following commands. Make sure maven is installed on the machine where you are running the shell script.

Go to the project directory and run the below command to build and compile the project.
1. $ sh bin/parking_lot.sh
This would start the build process and trigger the tests. Once build is successful you would get gojek-v1.jar in target folder.

2. i)To run the project in interactive CLI based input type, run the following command.
   $ sh bin/parking_lot.sh
   
   ii)To run the project with File as input type, run the following command. 
	  Make sure you have the file containing the command in the same directory as that of parking_lot.sh script. 
	$ sh bin/parking_lot.sh file_input.txt

Incase, any difficulites, kindly refer the screenshots attached. 