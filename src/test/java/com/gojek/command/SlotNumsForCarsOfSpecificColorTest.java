package com.gojek.command;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.ParkCarRepository;
import com.gojek.service.command.SlotNumbersOfCarsWithSpecificColour;

public class SlotNumsForCarsOfSpecificColorTest {

	
	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreatedAndParkCars();
	}


	@Test
	public void SlotNumsForCarsOfSpecificColorCommandTest() {
		
		SlotNumbersOfCarsWithSpecificColour slotNumbersOfCarsWithSpecificColour = new SlotNumbersOfCarsWithSpecificColour();
		slotNumbersOfCarsWithSpecificColour.executeCommand("slot_numbers_for_cars_with_colour White");
		assertEquals("Test failed", 2,
				ParkCarRepository.getCars().stream().filter(car -> "White".equals(car.getColour()))
						.map(car -> car.getSlotNumberAssigned()).collect(Collectors.toList()).size());

	}

}
