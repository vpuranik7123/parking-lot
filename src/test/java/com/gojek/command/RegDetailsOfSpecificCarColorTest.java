package com.gojek.command;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.ParkCarRepository;
import com.gojek.service.command.RegDetailsOfSpecificCarColour;

public class RegDetailsOfSpecificCarColorTest {


	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreatedAndParkCars();
	}

	
	@Test
	public void RegDetailsCommandTest() {
		
		RegDetailsOfSpecificCarColour reg = new RegDetailsOfSpecificCarColour();
		reg.executeCommand("registration_numbers_for_cars_with_colour White");
		assertEquals("RegDetails Test Failed", 2,
				ParkCarRepository.getCars().stream().filter(car -> "White".equals(car.getColour()))
						.map(car -> car.getRegistrationNumber()).collect(Collectors.toList()).size());

	}

}
