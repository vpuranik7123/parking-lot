package com.gojek.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.ParkCarRepository;
import com.gojek.service.command.Status;

public class StatusTest {
	
	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreatedAndParkCars();
		LeaveTest leaveTest = new LeaveTest();
		leaveTest.LeaveCommandTest();
	}
	
	
	@Test
	public void StatusCommandTest() {

		Status status = new Status();
		status.executeCommand("status");
		assertEquals("Status test failed", 5,ParkCarRepository.getCars().size());
	}

}
