package com.gojek.command;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	CreateParkingLotTest.class,
	ParkTest.class,
	LeaveTest.class,
	StatusTest.class,
	RegDetailsOfSpecificCarColorTest.class,
	SlotNumForRegNumTest.class,
	SlotNumsForCarsOfSpecificColorTest.class
	})
public class AllCommandSuite {
	


}
