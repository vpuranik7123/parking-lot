package com.gojek.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.ParkCarRepository;
import com.gojek.service.command.SlotNumberForSpecificRegNumber;

public class SlotNumForRegNumTest {

	
	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreatedAndParkCars();
	}



	@Test
	public void SlotNumForRegNumCommandTest() {
		

		SlotNumberForSpecificRegNumber slotNumberForSpecificRegNumber = new SlotNumberForSpecificRegNumber();
		slotNumberForSpecificRegNumber.executeCommand("slot_number_for_registration_number KA-01-HH-3141");
		assertEquals("Test failed", 6,
				ParkCarRepository.getCars().stream().filter(car -> "KA-01-HH-3141".equals(car.getRegistrationNumber()))
						.map(car -> car.getSlotNumberAssigned()).findFirst().get().intValue());

	}

}
