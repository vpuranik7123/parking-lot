package com.gojek.command;

import com.gojek.domainobject.ParkingLot;

public interface IParkingLot {

	static void createParkingLotIfNotCreated() {
		boolean isParkingLotCreated = ParkingLot.getParkingLotInstance() == null;
		if (isParkingLotCreated) {
			CreateParkingLotTest cpt = new CreateParkingLotTest();
			cpt.createParkingLotCommandTest();
		}
	}

	static void createParkingLotIfNotCreatedAndParkCars() {
		boolean isParkingLotCreated = ParkingLot.getParkingLotInstance() == null;
		if (isParkingLotCreated) {
			CreateParkingLotTest cpt = new CreateParkingLotTest();
			cpt.createParkingLotCommandTest();
			ParkTest park = new ParkTest();
			park.ParkCommandTest();
		}
	}
}
