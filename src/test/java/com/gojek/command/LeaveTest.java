package com.gojek.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.SlotRepository;
import com.gojek.service.command.Leave;

public class LeaveTest {
	
	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreatedAndParkCars();
	}

	@Test
	public void LeaveCommandTest() {
		
		Leave leave = new Leave();
		leave.executeCommand("leave 4");
		assertEquals("Error while leaving slot number 4 ", true, SlotRepository.getAvaliableSlot().contains(4));

	}

}
