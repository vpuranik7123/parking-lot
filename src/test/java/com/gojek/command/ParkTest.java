package com.gojek.command;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.gojek.repository.ParkCarRepository;
import com.gojek.service.command.Park;

public class ParkTest {
	
	@Before
	public void init() {
		IParkingLot.createParkingLotIfNotCreated();
	}
	
	@Test
	public  void ParkCommandTest() {
		
		Park park = new Park();
		park.executeCommand("park KA-01-HH-1234 White"); 
		park.executeCommand("park KA-01-HH-9999 White"); 
		park.executeCommand("park KA-01-BB-0001 Black"); 
		park.executeCommand("park KA-01-HH-7777 Red"); 
		park.executeCommand("park KA-01-HH-2701 Blue");
		park.executeCommand("park KA-01-HH-3141 Black");
		assertEquals("Failed to park cars",6, ParkCarRepository.getCars().size());
		
	}

}
