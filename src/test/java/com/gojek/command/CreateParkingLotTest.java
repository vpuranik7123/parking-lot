package com.gojek.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.gojek.domainobject.ParkingLot;
import com.gojek.service.command.CreateParkingLot;

public class CreateParkingLotTest {
	
	@Test
	public void createParkingLotCommandTest() {
		
		CreateParkingLot createParkingLot = new CreateParkingLot();
		createParkingLot.executeCommand("create_parking_lot 8");
		assertEquals("Failed to create parking lot with 8 slots",8, ParkingLot.getParkingLotInstance().getMaxSlots());
		
	}

}
