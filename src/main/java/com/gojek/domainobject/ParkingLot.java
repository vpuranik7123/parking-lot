package com.gojek.domainobject;

import java.io.ObjectStreamException;
import java.io.Serializable;


/**
 * @author Vinay Puranik
 *
 */
public class ParkingLot implements Serializable, Cloneable {

	private static volatile ParkingLot parkinglotInstance = null;
	private final int MAX_SLOTS;
	

	private ParkingLot(int maxSlots) {

		if (parkinglotInstance != null) {
			throw new RuntimeException("Cannot create, please use getParkingLotInstance()");
		}
		this.MAX_SLOTS = maxSlots;

	}

	public static void createParkingLotInstance(int maxSlots) {
		if (parkinglotInstance == null) {
			synchronized (ParkingLot.class) {
				if (parkinglotInstance == null) {
					parkinglotInstance = new ParkingLot(maxSlots);
				}
			}
		}
	}

	public static ParkingLot getParkingLotInstance() {
		return parkinglotInstance;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	private Object readResolve() throws ObjectStreamException {
		return parkinglotInstance;
	}

	
	public int getMaxSlots() {
		return this.MAX_SLOTS;
	}


}
