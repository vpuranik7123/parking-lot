package com.gojek.domainobject;


/**
 * @author Vinay Puranik
 *
 */
public class CarDO {
	
	private String registrationNumber;
	private int slotNumberAssigned;
	private String colour;
	
	
	public CarDO(String  registrationNumber, int slotNumberAssigned, String colour){
		this.registrationNumber = registrationNumber;
		this.slotNumberAssigned = slotNumberAssigned;
		this.colour = colour;
	}
	
	
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public int getSlotNumberAssigned() {
		return slotNumberAssigned;
	}
	public void setSlotNumberAssigned(int slotNumberAssigned) {
		this.slotNumberAssigned = slotNumberAssigned;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	
	
	

}
