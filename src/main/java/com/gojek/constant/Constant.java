package com.gojek.constant;


/**
 * @author Vinay Puranik
 *
 */
public class Constant {
	
	public static final String CREATE_PARKING_LOT = "create_parking_lot";
	public static final String EXIT="exit";
	public static final String INVALID = "invalid";
	public static final String LEAVE = "leave";
	public static final String PARK = "park";
	public static final String REG_DETAILS_SPECIFIC_CAR_COLOUR = "registration_numbers_for_cars_with_colour" ;
	public static final String SLOT_NUM_FOR_SPECIFIC_REG_NUM ="slot_number_for_registration_number";
	public static final String SLOT_NUMS_OF_CARS_SPECIFIC_COLOUR="slot_numbers_for_cars_with_colour";
	public static final String STATUS="status";
	public static final String METHOD_NAME = "executeCommand";
	public static final String PARKING_LOT_NOT_AVAILABLE = "Sorry, parking lot not available";
	public static final String SPACE = " ";
	public static final String EMPTY ="";
	public static final String COMMA_SPACE=", ";
	public static final String NOT_FOUND ="Not Found";


}
