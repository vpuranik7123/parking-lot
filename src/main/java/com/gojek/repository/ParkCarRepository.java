package com.gojek.repository;

import java.util.ArrayList;
import java.util.List;

import com.gojek.domainobject.CarDO;

/**
 * @author Vinay Puranik
 *
 */
public class ParkCarRepository {
	
	private static List<CarDO> cars = new ArrayList<>();
	
	public static void parkCar(CarDO car) {
		cars.add(car);
	}

	public static List<CarDO> getCars() {
		return cars;
	}

}
