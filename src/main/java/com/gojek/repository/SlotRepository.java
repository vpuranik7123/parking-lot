package com.gojek.repository;

import java.util.ArrayList;
import java.util.List;

import com.gojek.domainobject.ParkingLot;


/**
 * @author Vinay Puranik
 *
 */
public class SlotRepository {
	
	private static List<Integer> availableSlots = new ArrayList<>();

	public static List<Integer> getAvaliableSlot() {
		return availableSlots;
	}

	public static void setAvailableSlots(int maxSlot) {
		boolean isParkingLotInstanceCreated = ParkingLot.getParkingLotInstance() != null;
		if (isParkingLotInstanceCreated) {
			for (int i = 1; i <= maxSlot; i++) {
				availableSlots.add(i);
			}
		}
	}
	
}
