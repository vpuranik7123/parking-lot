package com.gojek.repository;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.gojek.constant.Constant;
import com.gojek.service.command.CreateParkingLot;
import com.gojek.service.command.Exit;
import com.gojek.service.command.Invalid;
import com.gojek.service.command.Leave;
import com.gojek.service.command.Park;
import com.gojek.service.command.RegDetailsOfSpecificCarColour;
import com.gojek.service.command.SlotNumberForSpecificRegNumber;
import com.gojek.service.command.SlotNumbersOfCarsWithSpecificColour;
import com.gojek.service.command.Status;


/**
 * @author Vinay Puranik
 *
 */
public class CommandRepository {

	public static Map<String, Method> commandMap = new HashMap<>();

	static {

		try {
			commandMap.put(Constant.CREATE_PARKING_LOT, CreateParkingLot.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.PARK, Park.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.REG_DETAILS_SPECIFIC_CAR_COLOUR,RegDetailsOfSpecificCarColour.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.SLOT_NUMS_OF_CARS_SPECIFIC_COLOUR,SlotNumbersOfCarsWithSpecificColour.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.SLOT_NUM_FOR_SPECIFIC_REG_NUM,SlotNumberForSpecificRegNumber.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.LEAVE, Leave.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.STATUS, Status.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.EXIT, Exit.class.getMethod(Constant.METHOD_NAME, String.class));
			commandMap.put(Constant.INVALID, Invalid.class.getMethod(Constant.METHOD_NAME, String.class));

		} catch (Exception e) {
			System.out.println("Error occured while loading commandMap");
		}

	}



}
