package com.gojek.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.gojek.constant.Constant;
import com.gojek.repository.CommandRepository;


/**
 * @author Vinay Puranik
 *
 */
public class ExecuteCommand {
	
	public static void execute(String command) {

		boolean isSpacePresent = command.contains(Constant.SPACE);
		String cmd = Constant.EMPTY;
		if (isSpacePresent) {
			cmd = command.substring(0, command.indexOf(Constant.SPACE));
		} else
			cmd = command;

		Method method = CommandRepository.commandMap.getOrDefault(cmd, CommandRepository.commandMap.get(Constant.INVALID));

		try {
			method.invoke(method.getDeclaringClass().newInstance(), command);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			System.out.println("Exception occured while executing command. Exception " + e.getMessage());
		}

	}

}
