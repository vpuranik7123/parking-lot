package com.gojek.service.input;


/**
 * @author Vinay Puranik
 *
 */
public interface IFileBased {
	
	void execute(String filename);

}
