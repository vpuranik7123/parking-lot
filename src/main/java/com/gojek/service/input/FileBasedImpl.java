package com.gojek.service.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.gojek.common.ExecuteCommand;


/**
 * @author Vinay Puranik
 *
 */
public class FileBasedImpl implements IFileBased {

	public void execute(String filename) {
		
		System.out.println(" You have chosen File based input type...");
		
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
			String command;
			while ((command = bufferedReader.readLine()) != null) {
				  ExecuteCommand.execute(command.trim());
			}

		} catch (IOException e) {
			System.out.print("Exception occured while reading from file. "+e.getMessage());
		} 

		
	}

}
