package com.gojek.service.input;


/**
 * @author Vinay Puranik
 *
 */
public interface IInteractiveCLIBased {
	
	void execute();

}
