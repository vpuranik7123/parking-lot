/**
 * 
 */
package com.gojek.service.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.gojek.common.ExecuteCommand;

/**
 * @author Vinay Puranik
 *
 */
public class InteractiveCLIBasedImpl implements IInteractiveCLIBased {

	public void execute() {

		System.out.println(" You have chosen Interactive CLI based input type.. type your commands now");
		
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
			String command;
			while ((command = bufferedReader.readLine()) != null) {
				ExecuteCommand.execute(command);
			}

		} catch (IOException e) {
			System.out.println("Exception occured while reading command from console. Exeception: " + e.getMessage());
		}

	}
	
	


}
