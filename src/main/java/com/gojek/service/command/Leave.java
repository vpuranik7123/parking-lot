package com.gojek.service.command;

import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;
import com.gojek.repository.SlotRepository;


/**
 * @author Vinay Puranik
 *
 */
public class Leave implements ICommand {

	@Override
	public void executeCommand(String command) {

		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;

		if (isParkingLotCreated) {
			int first = command.indexOf(Constant.SPACE);
			int slotNumber = Integer.parseInt(command.substring(first + 1));

			boolean isCarRemoved = ParkCarRepository.getCars().removeIf(car -> car.getSlotNumberAssigned() == slotNumber);

			if (isCarRemoved) {
				SlotRepository.getAvaliableSlot().add(slotNumber);
				System.out.println("Slot number " + slotNumber + " is free");
			} else
				System.out.println("Invalid slot to free");

		} else {
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);
		}

	}

}
