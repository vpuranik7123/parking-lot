package com.gojek.service.command;

import java.util.Optional;

import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;

/**
 * @author Vinay Puranik
 *
 */
public class SlotNumberForSpecificRegNumber implements ICommand {

	@Override
	public void executeCommand(String command) {
	
		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;
		
		if(isParkingLotCreated) {
			int first = command.indexOf(Constant.SPACE);
			String regNum = command.substring(first+1);
			Optional<Integer> optional = ParkCarRepository.getCars().stream().filter(car -> regNum.equals(car.getRegistrationNumber())).map(car -> car.getSlotNumberAssigned()).findAny();
			
			if(optional.isPresent()) {
				optional.ifPresent(System.out::println);
			}else
				System.out.println(Constant.NOT_FOUND);
			
		}else 
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);

	}

}
