package com.gojek.service.command;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;

/**
 * @author Vinay Puranik
 *
 */
public class SlotNumbersOfCarsWithSpecificColour implements ICommand {

	@Override
	public void executeCommand(String command) {
		
		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;
		
		if(isParkingLotCreated) {
			
			int first = command.indexOf(Constant.SPACE);
			String colour = command.substring(first+1);
			List<Integer> slotNumbers = ParkCarRepository.getCars().stream().filter(car -> colour.equals(car.getColour())).map(car -> car.getSlotNumberAssigned()).collect(Collectors.toList());
			boolean isAnyCarFound = slotNumbers.size() > 0;
			
			if(isAnyCarFound) {
				StringJoiner sj = new StringJoiner(Constant.COMMA_SPACE);
				slotNumbers.stream().forEach(slotNum -> sj.add(String.valueOf(slotNum)));
				System.out.println(sj.toString());
			}else {
				System.out.println(Constant.NOT_FOUND);
			}

		}else 
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);
		
		
	}

}
