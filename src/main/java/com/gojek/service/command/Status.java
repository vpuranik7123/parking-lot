package com.gojek.service.command;


import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;


/**
 * @author Vinay Puranik
 *
 */
public class Status implements ICommand {

	@Override
	public void executeCommand(String command) {
		
		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;
		
		if(isParkingLotCreated) {
			System.out.print("Slot No.");
			System.out.printf("%20s","Registration No");
			System.out.printf("%10s","Colour");
		
			ParkCarRepository.getCars().forEach( car -> { System.out.print("\n" +car.getSlotNumberAssigned());
			System.out.printf("%25s" , car.getRegistrationNumber());
			System.out.printf("%11s" , car.getColour());												
			});
			System.out.println();
			
		}else 
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);
		
	}



}
