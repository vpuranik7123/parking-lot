package com.gojek.service.command;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;


/**
 * @author Vinay Puranik
 *
 */
public class RegDetailsOfSpecificCarColour implements ICommand {

	@Override
	public void executeCommand(String command) {
		
		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;
		
		
		if(isParkingLotCreated) {
			int first = command.indexOf(Constant.SPACE);
			String color = command.substring(first+1);
			List<String> regNumbers = ParkCarRepository.getCars().stream().filter(car -> color.equals(car.getColour())).map(car -> car.getRegistrationNumber()).collect(Collectors.toList());
			boolean isAnyCarFound = regNumbers.size() > 0;
			
			if(isAnyCarFound) {
				StringJoiner sj = new StringJoiner(Constant.COMMA_SPACE);
				regNumbers.stream().forEach(regNum -> sj.add(regNum));
				System.out.println(sj.toString());
			}else {
				System.out.println(Constant.NOT_FOUND);
			}
			
			
		}else 
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);
		

	}

}
