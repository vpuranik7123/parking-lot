package com.gojek.service.command;

import com.gojek.constant.Constant;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.SlotRepository;


/**
 * @author Vinay Puranik
 *
 */
public class CreateParkingLot implements ICommand {

	@Override
	public void executeCommand(String command) {

		try {
			int maxSlot = Integer.parseInt(command.substring(command.indexOf(Constant.SPACE) + 1));
			ParkingLot.createParkingLotInstance(maxSlot);
			SlotRepository.setAvailableSlots(maxSlot);
			System.out.println("Created a parking lot with " + maxSlot + " slots.");
		} catch (Exception e) {
			System.out.println("Exception occured while creating parking lot "+e.getMessage());
		}

	}

}
