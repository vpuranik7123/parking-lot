package com.gojek.service.command;


/**
 * @author Vinay Puranik
 *
 */
public class Exit implements ICommand {

	@Override
	public void executeCommand(String command) {
		
		System.out.println("Terminating..");
		System.exit(0);

	}

}
