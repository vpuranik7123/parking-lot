package com.gojek.service.command;

import com.gojek.constant.Constant;
import com.gojek.domainobject.CarDO;
import com.gojek.domainobject.ParkingLot;
import com.gojek.repository.ParkCarRepository;
import com.gojek.repository.SlotRepository;


/**
 * @author Vinay Puranik
 *
 */
public class Park implements ICommand {

	@Override
	public void executeCommand(String command) {

		ParkingLot parkingLot = ParkingLot.getParkingLotInstance();
		boolean isParkingLotCreated = parkingLot != null;

		if (isParkingLotCreated) {
			
			int first = command.indexOf(Constant.SPACE); // first occurrence of space
			int second = command.indexOf(Constant.SPACE, first + 1); // second occurrence of the space
			String registrationNumber = command.substring(first + 1, second).trim();
			String colour = command.substring(second).trim();
			int slotNumber = assignSlotNumber(parkingLot);
			boolean isSlotFree = slotNumber > 0;

			if (isSlotFree) {
				CarDO car = new CarDO(registrationNumber, slotNumber, colour);
				ParkCarRepository.parkCar(car);
				System.out.println("Allocated slot number: " + slotNumber);
			} else {
				System.out.println("Sorry, parking lot is full");
			}
			
		}else {
			System.out.println(Constant.PARKING_LOT_NOT_AVAILABLE);
		}

	}
	
	
	private int assignSlotNumber(ParkingLot parkingLot) {
		
		boolean isSlotFree = SlotRepository.getAvaliableSlot().size() > 0;
		
		if(isSlotFree) {
			int freeSlot = SlotRepository.getAvaliableSlot().stream().sorted().findFirst().get();
			SlotRepository.getAvaliableSlot().removeIf(x -> x == freeSlot);
			return freeSlot;
			
		}else {
			return -1;
		}
		
		
	}
	
	

}
