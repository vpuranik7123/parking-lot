package com.gojek.service.command;


/**
 * @author Vinay Puranik
 *
 */
public interface ICommand {
	
	void executeCommand(String command);

}
