package com.gojek.service.command;


/**
 * @author Vinay Puranik
 *
 */
public class Invalid implements ICommand {

	@Override
	public void executeCommand(String command) {
		
		System.out.println("Invalid command :"+command);

	}

}
