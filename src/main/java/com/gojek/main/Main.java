package com.gojek.main;

import com.gojek.service.input.FileBasedImpl;
import com.gojek.service.input.InteractiveCLIBasedImpl;


/**
 * @author Vinay Puranik
 *
 */
public class Main {

	public static void main(String[] args) {

		boolean isinputFileType = args.length > 0;

		if (isinputFileType) {
			new FileBasedImpl().execute(args[0]);
		} else {
			new InteractiveCLIBasedImpl().execute();
		}

	}

}
